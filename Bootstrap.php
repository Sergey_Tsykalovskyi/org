<?php

namespace rokittd\tasks;

use \yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app){
        $app->controllerMap['tasks'] = '\rokittd\TasksController';
    }
}
